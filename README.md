pluma-codefolding
==================

Forked repository: [link](https://github.com/satyajitc/gedit3-codefolding)

---

This plugin adds code folding support to gedit 3.x. It supports several (meaning more than 1) languages. Note that this plugin is <b>not</b> compatible with gedit 2.x.

## Features
Besides folding indivudual blocks by clicking the respective block start markers on the left margin, two other operations are supported either from the Tools menu or from the keyboard like this
- **(BROKEN, ONLY FOLDS THE FIRST BLOCK)** <kbd>Alt</kbd>+<kbd>Shift</kbd>+<kbd>T</kbd>: Toggle all blocks
- **(DO NOT USE, WILL CAUSE FREEZE)** <kbd>Alt</kbd>+<kbd>Shift</kbd>+<kbd>C</kbd>: Fold current block

## Suported languages
- C/C++
- CSS
- Graphviz Dot
- Java
- Javascript
- JSON
- Lua
- Objective-C
- Perl
- Prolog
- R
- Ruby
- sh
- Scheme
- SQL
- XML

## Installation
Copy both the files from the src directory of this repo & place in `.local/share/pluma/plugins/` directory.

## Notes
- This plugin is a very simple hack for Pluma support. It is functional, but there are some bugs, and there is no visual indicator whatsoever; YMMV, but it is enough for my personal use cases
- Code folding is dependent on properly formatted blocks of code. If the folding behaves erratic try reformatting the code
- A simple way of reformatting to facilitate folding is to have block start and end constructs in seperate individual lines
